<?php namespace Multoo\Router\Mapper\Alexssssss\OrmModel;

abstract class AbstractRouteEntity extends \Alexssssss\OrmModel\Entity\AbstractEntity implements \Multoo\Router\RouteInterface
{

    protected $id;
    public $path;
    public $regex;
    public $namedRegexMatches;
    public $load;
    public $module;
    public $section;

    public function __construct(AbstractRouteService $route)
    {
        parent::__construct($route);
    }

    public function getLoad()
    {
        return $this->load;
    }

    public function getModule()
    {
        return $this->module;
    }

    public function getSection()
    {
        return $this->section;
    }

    public function getParams($request)
    {
        $params = [];
        if ($this->regex == true && !empty($this->namedRegexMatches)) {
            $keys = json_decode($this->namedRegexMatches);

            if (is_object($keys)) {
                preg_match('/' . str_replace('/', '\\/', $this->path) . '/i', $request, $matches);

                foreach ($keys as $key => $fieldname) {
                    $params[$fieldname] = $matches[$key];
                }
            }
        }

        return $params;
    }
}
