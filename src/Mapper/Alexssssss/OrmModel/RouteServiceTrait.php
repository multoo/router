<?php

namespace Multoo\Router\Mapper\Alexssssss\OrmModel;

trait RouteServiceTrait
{
    public function getByRequest($request, array $params)
    {
        return $this->getOne('(regex = 0 AND path = :path) OR (regex = 1 AND :path REGEXP path)', ['path' => $request]);
    }
}
