CREATE TABLE `routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(32) NOT NULL,
  `path` varchar(64) NOT NULL,
  `regex` int(1) NOT NULL DEFAULT '0',
  `namedRegexMatches` varchar(64) DEFAULT NULL,
  `load` varchar(45) DEFAULT NULL,
  `module` varchar(45) DEFAULT NULL,
  `section` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `path_UNIQUE` (`app`,`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;