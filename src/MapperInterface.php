<?php

namespace Multoo\Router;

interface MapperInterface
{

    /**
     * @return \Multoo\Router\EntityInterface
     */
    public function getByRequest($request, array $params);
}
