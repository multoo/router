<?php

namespace Multoo\Router;

class Route
{

    /**
     *
     * @var \Multoo\Router\MapperInterface
     */
    protected $mapper;

    public function __construct(\Multoo\Router\MapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    public function run($request, array $params = array())
    {
        return ($entity = $this->mapper->getByRequest($request, $params)) ? $this->processEntity($entity, $request) : false;
    }

    protected function processEntity(\Multoo\Router\RouteInterface $entity, $request)
    {
        $route = [
            'module' => $entity->getModule(),
            'section' => $entity->getSection(),
            'load' => $entity->getLoad(),
            'request' => $request
        ];

        $route = array_merge($route, $entity->getParams($request));

        if (isset($route['module']) && ($parts = explode('/', $route['section']))) {
            if (count($parts) > 1) {
                $route['section'] = array_pop($parts);

                if (count($parts)) {
                    $route['module'] .= '/' . implode('/', $parts);
                }
            }
        }

        return $route;
    }
}
