<?php

namespace Multoo\Router;

interface RouteInterface
{

    public function getModule();

    public function getSection();

    public function getLoad();

    public function getParams($request);
}
