Router
====
| Branch  | GitLab CI Status                                                                                                                  |
|---------|-----------------------------------------------------------------------------------------------------------------------------------|
| Master  | [![build status](https://ci.gitlab.com/projects/1866/status.png?ref=master)](https://ci.gitlab.com/projects/1866?ref=master)      |
| Develop | [![build status](https://ci.gitlab.com/projects/1866/status.png?ref=develop)](https://ci.gitlab.com/projects/1866?ref=develop)    |