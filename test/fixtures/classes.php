<?php

class RouteFixture implements \Multoo\Router\RouteInterface
{

    public function getModule()
    {
        return 'module';
    }

    public function getSection()
    {
        return 'section';
    }

    public function getLoad()
    {
        return 'load';
    }

    public function getParams($request)
    {
        return ['arg1' => 'val1'];
    }
}

class MapperFixture implements \Multoo\Router\MapperInterface
{

    public function getByRequest($request, array $params)
    {
        if (!empty($request)) {
            return new RouteFixture();
        } else {
            return false;
        }
    }
}
